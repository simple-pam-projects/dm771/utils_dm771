#!/usr/bin/env bash

keytool -genkeypair -alias eapmgmt -storetype jks -keyalg RSA -keysize 2048 -keypass s4f3p455 -keystore fife.jks -storepass s4f3p455 -dname "CN=BusinessCentral,OU=RegionMidt,O=Stybje,L=Global,ST=DK,C=DK" -validity 1825 -v
mv fife.jks pam7/standalone/configuration

echo "-"
echo "- Preparing SSL for applications"
echo "-"

rm -f cli.*
cat << __EOF > cli.3
embed-server
/core-service=management/security-realm=ApplicationRealm/server-identity=ssl:remove
/core-service=management/security-realm=ApplicationRealm/server-identity=ssl:add(keystore-path=fife.jks,keystore-relative-to=jboss.server.config.dir,keystore-password=s4f3p455,alias=eapmgmt)
stop-embedded-server
__EOF
cp cli.3 pam7/bin/cli.final
pushd pam7/bin &> /dev/null
  ./jboss-cli.sh --file=cli.final
popd &> /dev/null

echo "-"
echo "- Preparing SSL for management"
echo "-"

rm -f cli.*
cat << __EOF > cli.1
embed-server
/core-service=management/management-interface=http-interface:write-attribute(name=secure-socket-binding, value=management-https)

/core-service=management/management-interface=http-interface:undefine-attribute(name=socket-binding)
__EOF

: > pam7/standalone/configuration/https-mgmt-users.properties

cat << __EOF > cli.2
/core-service=management/security-realm=ManagementRealmHTTPS:add

/core-service=management/security-realm=ManagementRealmHTTPS/authentication=properties:add(path=https-mgmt-users.properties,relative-to=jboss.server.config.dir)

/core-service=management/management-interface=http-interface:write-attribute(name=security-realm,value=ManagementRealmHTTPS)

/core-service=management/security-realm=ManagementRealmHTTPS/server-identity=ssl:add(keystore-path=fife.jks,keystore-relative-to=jboss.server.config.dir,keystore-password=s4f3p455, alias=eapmgmt)

if (outcome == success) of /system-property=javax.net.ssl.trustStore:read-resource
  /system-property=javax.net.ssl.trustStore:remove 
end-if
/system-property=javax.net.ssl.trustStore:add(value=\${jboss.server.config.dir}/fife.jks) 

if (outcome == success) of /system-property=javax.net.ssl.trustStorePassword:read-resource
  /system-property=javax.net.ssl.trustStorePassword:remove 
end-if
/system-property=javax.net.ssl.trustStorePassword:add(value=s4f3p455)

stop-embedded-server
__EOF

: > cli.final
cat cli.1 >> cli.final
cat cli.2 >> cli.final
cp cli.final pam7/bin
pushd pam7/bin &> /dev/null
  ./jboss-cli.sh --file=cli.final
  ./add-user.sh  -up ../standalone/configuration/https-mgmt-users.properties -r ManagementRealmHTTPS -s --user "admin" --password "S3cr3tK3y#"
popd &> /dev/null

#
# Inform Decision Central of the changes
#
echo "org.uberfire.ext.security.management.wildfly.cli.port=9993" >> pam7/standalone/deployments/decision-central.war/WEB-INF/classes/security-management.properties

echo "datasource.management.wildfly.host=localhost" >> pam7/standalone/deployments/decision-central.war/WEB-INF/classes/datasource-management.properties
echo "datasource.management.wildfly.port=9993" >> pam7/standalone/deployments/decision-central.war/WEB-INF/classes/datasource-management.properties
echo "datasource.management.wildfly.admin=admin" >> pam7/standalone/deployments/decision-central.war/WEB-INF/classes/datasource-management.properties
echo "datasource.management.wildfly.password=S3cr3tK3y#" >> pam7/standalone/deployments/decision-central.war/WEB-INF/classes/datasource-management.properties
echo "datasource.management.wildfly.realm=ManagementRealmHTTPS" >> pam7/standalone/deployments/decision-central.war/WEB-INF/classes/datasource-management.properties
echo "datasource.management.wildfly.protocol=https-remoting" >> pam7/standalone/deployments/decision-central.war/WEB-INF/classes/datasource-management.properties


echo "-"
echo "- HTTPS for Management interface and applications is now active"
echo "-"

