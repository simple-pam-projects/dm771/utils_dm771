#!/usr/bin/env bash

PAM_HOME=$PWD/pam7

mv oban.jks $PAM_HOME/standalone/configuration

rm -f cli.*
cat << __EOF > cli.3
embed-server
/core-service=management/security-realm=ApplicationRealm/server-identity=ssl:remove
/core-service=management/security-realm=ApplicationRealm/server-identity=ssl:add(keystore-path=oban.jks,keystore-relative-to=jboss.server.config.dir,keystore-password=s4f3p455,alias=eapmgmt)
stop-embedded-server
__EOF
cp cli.3 $PAM_HOME/bin/cli.final
pushd $PAM_HOME/bin &> /dev/null
  ./jboss-cli.sh --file=cli.final
  rm -f cli.final
popd &> /dev/null

rm -f cli.*
cat << __EOF > cli.1
embed-server
/core-service=management/management-interface=http-interface:write-attribute(name=secure-socket-binding, value=management-https)

/core-service=management/management-interface=http-interface:undefine-attribute(name=socket-binding)
__EOF

: > $PAM_HOME/standalone/configuration/https-mgmt-users.properties

cat << __EOF > cli.2
/core-service=management/security-realm=ManagementRealmHTTPS:add

/core-service=management/security-realm=ManagementRealmHTTPS/authentication=properties:add(path=https-mgmt-users.properties,relative-to=jboss.server.config.dir)

/core-service=management/management-interface=http-interface:write-attribute(name=security-realm,value=ManagementRealmHTTPS)

/core-service=management/security-realm=ManagementRealmHTTPS/server-identity=ssl:add(keystore-path=oban.jks,keystore-relative-to=jboss.server.config.dir,keystore-password=s4f3p455, alias=eapmgmt)

if (outcome == success) of /system-property=javax.net.ssl.trustStore:read-resource
  /system-property=javax.net.ssl.trustStore:remove 
end-if
/system-property=javax.net.ssl.trustStore:add(value=\${jboss.server.config.dir}/oban.jks) 

if (outcome == success) of /system-property=javax.net.ssl.trustStorePassword:read-resource
  /system-property=javax.net.ssl.trustStorePassword:remove 
end-if
/system-property=javax.net.ssl.trustStorePassword:add(value=s4f3p455)

stop-embedded-server
__EOF

: > cli.final
cat cli.1 >> cli.final
cat cli.2 >> cli.final
cp cli.final $PAM_HOME/bin
pushd $PAM_HOME/bin &> /dev/null
  ./jboss-cli.sh --file=cli.final
  ./add-user.sh  -up ../standalone/configuration/https-mgmt-users.properties -r ManagementRealmHTTPS -s --user "admin" --password "S3cr3tK3y#"
  rm -f cli.final
popd &> /dev/null

#
# Inform Decision Central of the changes
#
if [[ -d $PAM_HOME/standalone/deployments/decision-central.war ]]; then
  echo "org.uberfire.ext.security.management.wildfly.cli.port=9993" >> $PAM_HOME/standalone/deployments/decision-central.war/WEB-INF/classes/security-management.properties
  
  echo "datasource.management.wildfly.host=localhost" >> $PAM_HOME/standalone/deployments/decision-central.war/WEB-INF/classes/datasource-management.properties
  echo "datasource.management.wildfly.port=9993" >> $PAM_HOME/standalone/deployments/decision-central.war/WEB-INF/classes/datasource-management.properties
  echo "datasource.management.wildfly.admin=admin" >> $PAM_HOME/standalone/deployments/decision-central.war/WEB-INF/classes/datasource-management.properties
  echo "datasource.management.wildfly.password=S3cr3tK3y#" >> $PAM_HOME/standalone/deployments/decision-central.war/WEB-INF/classes/datasource-management.properties
  echo "datasource.management.wildfly.realm=ManagementRealmHTTPS" >> $PAM_HOME/standalone/deployments/decision-central.war/WEB-INF/classes/datasource-management.properties
  echo "datasource.management.wildfly.protocol=https-remoting" >> $PAM_HOME/standalone/deployments/decision-central.war/WEB-INF/classes/datasource-management.properties
fi

echo "-"
echo "- SSL certificate for Management and Applications installed"
echo "-"

