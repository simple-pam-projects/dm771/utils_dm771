# Importing ColumnaDynamicRulesMidt repository

Through the following steps the `ColumnaDynamicRulesMidt_stathis` repository has been successfully imported into Business Central of RHDM.7.7.1

**Extract files from bundle `ColumnaDynamicRulesMidt_stathis`**

Extract files and prepare directory

```
unzip ColumnaDynamicRulesMidt_stathis.zip
cd ColumnaDynamicRulesMidt_stathis
rm -rf .git
```

**Create empty git repo**

Login to gitlab  (in my case `gitea`) and create empty git repository, for example `coldynarules`

>IMPORTANT: The directory must be completely empty with no files created, such as `.gitignore`, `README.md` or any other file that the git repository manager might offer to created. 

**Import files from `ColumnaDynamicRulesMidt_stathis` directory into the `coldynarules` repository**

While in the `ColumnaDynamicRulesMidt_stathis` directory do the following steps:

```
git init
git add .
git commit -m"Initial commit"
git remote add origin http://192.168.0.100:3000/erouvas/coldynarules.git   <-- SHOULD BE REPLACED WITH THE URL FROM GITLAB
git push -u origin master
```

After successful completion of the `git push` command the project files should be visible at the gitlab repository (use a browser to verify this)


**Import project into Business Central**

Login to Business Central of RHDM.7.7.1 as `pamAdmin` and select «Import Project». In the dialog that pops up, fill in the «URL» field with the url of the project as provided by gitlab and in the «Authentication Options» fill in with the credentials of a user that has permissions to access the gitlab project.

In the following dialog select the project and press «OK».

The project should be imported.


> Written with [StackEdit](https://stackedit.io/).
