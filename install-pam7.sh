#!/bin/bash

#
# ---------------
# ---         ---  Executing this script will DELETE
# --- WARNING ---  any previous installations
# ---         ---  of RHPAM and RHDM
# ---------------  in the current directory
#
#
# After installation is finished, modify file default.conf and copy it 
# to $INSTALL_DIR/git-hooks
#
# The default.conf file is used by the post-commit git hooks
# to integrate Business Central with a remote git repository manager
#

OPTIONS=""

INSTALL_DIR=pam7
#RUN_MODE=production
RUN_MODE=development
GIT_HOOK=bcgithook
GIT_HOOK_LOCATION=download

JVM_MEMORY=2048

OPTIONS="$OPTIONS:git_hook=$GIT_HOOK:git_hook_location=$GIT_HOOK_LOCATION"
OPTIONS="$OPTIONS:install_dir=$INSTALL_DIR"
OPTIONS="$OPTIONS:jvm_memory=$JVM_MEMORY"
OPTIONS="$OPTIONS:run_mode=$RUN_MODE"
OPTIONS="$OPTIONS:logfile=/tmp/pam7.log"
OPTIONS="$OPTIONS:node1_config=addons/access_log"

#
# - clean previous installation
#
rm -rf go_pam* pam gopam.sh jboss-eap-7.2 rh-sso-7.3 gonode*.sh gosso.sh pam-config.db gostandalone.sh "$INSTALL_DIR"
rm -rf jboss-eap-7.3 

#
# - install 
#
bash -e ./pam-setup.sh -n localhost -b both -o "$OPTIONS"

echo
echo "Installation finished in $SECONDS seconds"
echo

