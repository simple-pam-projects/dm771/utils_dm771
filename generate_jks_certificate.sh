#!/usr/bin/env bash

keytool -genkeypair -alias eapmgmt -storetype jks \
        -keyalg RSA -keysize 2048 -keypass s4f3p455 \
        -keystore oban.jks -storepass s4f3p455 \
        -dname "CN=BusinessCentral,OU=RegionMidt,O=Stybje,L=Global,ST=DK,C=DK" \
        -validity 1825 -v

