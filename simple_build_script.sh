#!/usr/bin/env bash

#
# - this is a simplistic attempt to build a KIE-based project
#   stored in a git repository
#
#   ---------------
#   ---         ---  This script takes a *very* simplistic view of the 
#   --- WARNING ---  required procedure
#   ---         ---  Should only be considered as a temporary approach
#   ---------------
#
#   Steps to be taken:
#   (1) git clone a project
#   (2) update mvn version of the project with current timestamp
#   (3) execute a mvn deploy
#
#   WILL NOT PUSH BACK TO ORIGINAL GIT REPO
#   VERSION OF ORGINAL GIT REPO WILL REMAIN UNCHANGED
#

#
# --- CONFIGURATION ---
#

GIT_PROJECT_URL="$1"
GIT_USER_NAME=erouvas
GIT_USER_PASS='pZ$Xx55@pA4TeYzUyGe#'

MAVEN_OPTS=""
MAVEN_OPTS="$MAVEN_OPTS -Dproject.build.sourceEncoding=UTF-8 "
MAVEN_OPTS="$MAVEN_OPTS -Dmaven.compiler.source=1.8 -Dmaven.compiler.target=1.8"
MAVEN_OPTS="$MAVEN_OPTS -DaltReleaseDeploymentRepository=nexus_local_release::default::http://192.168.0.100:8081/repository/maven-releases/ "
MAVEN_OPTS="$MAVEN_OPTS -DaltSnapshotDeploymentRepository=nexus_local_snapshot::default::http://192.168.0.100:8081/repository/maven-snapshots/ "
MAVEN_OPTS="$MAVEN_OPTS --settings=/home/erouvas/main/rh/pelatis/region.Midt/pam.regionMidt/settings.xml"

[[ -z "$GIT_PROJECT_URL" ]] && { echo >&2 'ERROR: GIT PROJECT URL not specified - Aborting'; exit 1; }
[[ -z "$GIT_USER_NAME" ]]   && { echo >&2 'ERROR: GIT USER NAME not specified - Aborting'; exit 1; }
[[ -z "$GIT_USER_PASS" ]]   && { echo >&2 'ERROR: GIT USER PASSWORD not specified - Aborting'; exit 1; }

#
# -- END OF CONFIGURATION ---
#

#
# -- ENVIRONMENT CHECK --
#

#
# sanity environment check
#
CYGWIN_ON=no
MACOS_ON=no
LINUX_ON=no
min_bash_version=4

# - try to detect CYGWIN
# a=`uname -a` && al=${a,,} && ac=${al%cygwin} && [[ "$al" != "$ac" ]] && CYGWIN_ON=yes
# use awk to workaround MacOS bash version
a=$(uname -a) && al=$(echo "$a" | awk '{ print tolower($0); }') && ac=${al%cygwin} && [[ "$al" != "$ac" ]] && CYGWIN_ON=yes
if [[ "$CYGWIN_ON" == "yes" ]]; then
  echo "CYGWIN DETECTED - WILL TRY TO ADJUST PATHS"
  min_bash_version=4
fi

# - try to detect MacOS
a=$(uname) && al=$(echo "$a" | awk '{ print tolower($0); }') && ac=${al%darwin} && [[ "$al" != "$ac" ]] && MACOS_ON=yes
[[ "$MACOS_ON" == "yes" ]] && min_bash_version=5 && echo "macOS DETECTED"

# - try to detect Linux
a=$(uname) && al=$(echo "$a" | awk '{ print tolower($0); }') && ac=${al%linux} && [[ "$al" != "$ac" ]] && LINUX_ON=yes
[[ "$LINUX_ON" == "yes" ]] && min_bash_version=4

bash_ok=no && [ "${BASH_VERSINFO:-0}" -ge $min_bash_version ] && bash_ok=yes
[[ "$bash_ok" != "yes" ]] && echo "ERROR: BASH VERSION NOT SUPPORTED - PLEASE UPGRADE YOUR BASH INSTALLATION - ABORTING" && exit 1 

#
# sanity checks on environment environment
#
command -v sed &> /dev/null      || { echo >&2 'ERROR: sed not installed. Please install sed.4.2 (or later, gnu sed.4.8 for macOS) to continue - Aborting'; exit 1; }
command -v java &> /dev/null     || { echo >&2 'ERROR: JAVA not installed. Please install JAVA.8 to continue - Aborting'; exit 1; }
command -v unzip &> /dev/null    || { echo >&2 'ERROR: UNZIP not installed. Please install UNZIP to continue - Aborting'; exit 1; }
command -v sqlite3 &> /dev/null  || { echo >&2 'ERROR: SQLite not installed. Please install SQLite to continue - Aborting'; exit 1; }
command -v grep &> /dev/null     || { echo >&2 'ERROR: grep not installed. Please install grep to continue - Aborting'; exit 1; }
command -v awk &> /dev/null      || { echo >&2 'ERROR: awk not installed. Please install awk to continue - Aborting'; exit 1; }
command -v basename &> /dev/null || { echo >&2 'ERROR: basename not installed. Please install basename to continue - Aborting'; exit 1; }
# required to checkout and built dependencies
command -v curl &> /dev/null     || { echo >&2 'ERROR: curl not installed. Please install curl to continue - Aborting'; exit 1; }
command -v git &> /dev/null      || { echo >&2 'ERROR: GIT not installed. Please install GIT.1.8 (or later) to continue - Aborting'; exit 1; }
command -v mvn &> /dev/null      || { echo >&2 'ERROR: MAVEN not installed. Please install MAVEN.3.6.2 (or later) to continue - Aborting'; exit 1; }

# - check mvn version
mvnVersion=$(mvn -version | head -1 | awk '{print $3}' | tr -d '.')
[[ "$mvnVersion" -lt "362" ]]  && { echo >&2 "WARNING: MAVEN version($mvnVersion) too old. Please consider upgrading to version 3.6.2 (or later)"; }
unset mvnvVersion

# - check java version
tmp=$(java -XshowSettings:all -version 2>&1| grep version | grep specification | grep -v vm | awk -F'=' '{print $2}')
tmp="${tmp#"${tmp%%[![:space:]]*}"}" && tmp="${tmp%"${k##*[![:space:]]}"}"
javaspec="$tmp"
goon=no && ( [[ "$javaspec" == "1.8" ]] || [[ "$javaspec" == "11" ]] ) && goon=yes
[[ "$goon" == "no"  ]] && { echo >&2 "ERROR: JAVA VERSION not supported. Please install version 8 or 11, found version $javascped - Aborting"; exit 1; }
unset tmp javaspec goon

# - check sed version on Mac
if [[ "$MACOS_ON" == "yes" ]]; then
  macsed=no
  macstatus=256
  sed --version &> sed.out; macstatus=$?
  if [[ "$macstatus" -eq 0 ]]; then
    maxv=$(sed --version | head -1 | awk '{print $NF}' | cut -d'.' -f 1)
    minv=$(sed --version | head -1 | awk '{print $NF}' | cut -d'.' -f 2)
    [[ "$maxv" -ge "4" ]] && [[ "$minv" -ge "8" ]] && macsed=yes
    unset maxv minv
  else
    macsed=no
  fi
  [[ "$macsed" == "yes" ]] || { echo >&2 'ERROR: GNU sed not found. Please install GNU sed.4.8 (or latest) to continue - Aborting'; exit 2; }
  unset macsed macstatus
  rm -f sed.out
fi

#
# - DONE CHECKING ENVIRONMENT -
#

#
# - DEFINE FUNCTIONS
#
urlencode() {
  old_lc_collate="$LC_COLLATE"
  LC_COLLATE=C

  local length="${#1}"
  for (( i = 0; i < length; i++ )); do
      local c="${1:i:1}"
      case $c in
          [a-zA-Z0-9.~_-]) printf "%s" "$c" ;;
          *) printf '%%%02X' "'$c" ;;
      esac
  done

  LC_COLLATE="$old_lc_collate"
}

urldecode() {
  local url_encoded="${1//+/ }"
  printf '%b' "${url_encoded//%/\\x}"
}

updateMvnVersion() {
  local VERSION_BASE=$(date +%F | tr -d '-')
  
  local mvnCounter=0;
  local mvnFound=no
  mvnVersion=${VERSION_BASE}
  local mvnCounterPadded="00"$mvnCounter
  while [[ "x$mvnFound" == "xno" ]]; do
    mvnCounter=$((mvnCounter+1))
    mvnCounterPadded="00"$mvnCounter
    mvnVersion=${VERSION_BASE}
    mvnVersion="${mvnVersion}.${mvnCounterPadded: -3}"
    if [[ ! -r /tmp/mvnVersion.$mvnVersion ]]; then
      mvnFound=yes
      : > /tmp/mvnVersion.$mvnVersion
    fi
  done
  mvnVersion=${mvnVersion^^}
  mvn versions:set -DnewVersion=$mvnVersion -DgenerateBackupPoms=false "$@"
  find . -iname '*.versionsBackup' -exec rm {} \;
}
#
# - END OF FUNCTION DEFINITION
#


PRJ_NAME=null && PRJ_NAME="$(basename ${GIT_PROJECT_URL} .git)"
[[ -z "$PRJ_NAME" ]] &&  { echo >&2 'ERROR: Project name is empty - Aborting'; exit 1; }

workusername=$(urlencode "$GIT_USER_NAME")
workpasswd=$(urlencode "$GIT_USER_PASS")
useme=""
bburl=""
gitUrl="$GIT_PROJECT_URL"
isit="https://" && checkurl=${gitUrl#$isit} && [[ "$checkurl" != "$gitUrl" ]] && useme="$isit"
isit="http://"  && checkurl=${gitUrl#$isit} && [[ "$checkurl" != "$gitUrl" ]] && useme="$isit"
[[ "$isit" != "" ]] && bburl=${gitUrl#$useme} && bburl=${bburl/*@/} && bburl="${useme}$workusername:$workpasswd@$bburl"

rm -rf "$PRJ_NAME"
git clone "$bburl"
[[ ! -d "$PRJ_NAME" ]] &&  { echo >&2 'ERROR: Cannot find cloned directory for project - Aborting'; exit 1; }

pushd "$PRJ_NAME" &> /dev/null
  updateMvnVersion
  mvn clean deploy $MAVEN_OPTS
  PRJ_GAV=$(mvn -q -Dexec.executable=echo -Dexec.args='${project.groupId}:${project.artifactId}:${project.version}' --non-recursive exec:exec 2>/dev/null)
popd &> /dev/null

echo "::"
echo ":: NEW ARTEFACT MAVEN VERSION IS $mvnVersion"
echo "::"
echo ":: COMPLETE ARTEFACT MAVEN VECTOR IS $PRJ_GAV"
echo "::"
echo ":: WARNING: NOT PUSHING TO ORIGINAL GIT REPO"
echo "::          GIT REPO VERSION WILL REMAIN UNCHANGED"
echo "::"

