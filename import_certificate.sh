#!/usr/bin/env bash

cp /etc/pki/ca-trust/extracted/java/cacerts cacerts.bc
chmod u+w cacerts.bc
rm -f bc_server.pem
openssl s_client -showcerts -connect localhost:8443  </dev/null 2>/dev/null|openssl x509 -outform PEM > bc_server.pem
keytool -import -trustcacerts -keystore cacerts.bc -storepass changeit -noprompt -alias bc_ssl -file bc_server.pem

echo 
echo "Add updated certificate to RHDM startup with"
echo
echo "  -Djavax.net.ssl.trustStore=$PWD/cacerts.bc"
echo
