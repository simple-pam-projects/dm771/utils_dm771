# Simple Build Script and Deployment to KIE Containers

Lacking a proper CI/CD environment a simple build script, named `simple_build_script.sh`, has been developed to automate the build process of a KIE project and can be found at:

* https://gitlab.com/simple-pam-projects/dm771/utils_dm771/-/blob/master/simple_build_script.sh

A companion script, named `update-rules.js`, has also been developed to deploy a KJAR to managed KIE Servers through a Decision Central (or a headless controller). The script can be found at:

* https://gitlab.com/simple-pam-projects/dm771/utils_dm771/-/blob/master/update-rules.js

> **NOTE**:  These scripts offer a bare minimum functionality and should only be regarded as a temporal approach until a more sophisticated solution is put into place

## Overview of the Simple Build Script

The script will perform the following steps:

* clone a project from git based on a provided URL
* update the version of the project using a timestamp format of "YYYYMMDD.build", where
	* *YYYY* is the current year, eg "2021"
	* *MM* is the current month, eg "02"
	* *DD* is the curremtn day, eg "14"
	* *build* is a number that is increased on every build on the same day, e.g. "001", "002", etc

The updated version is **NOT** pushed back to the git repository so the original project version does not change. By not pushing the version change back to the git repository the project can continue to be edited in the Decision Central after the build. If the change was pushed back the project would have to be deleted from Decision Central and imported anew from the git repository.

* invoke maven to build the project
* attempt to publish (`deploy`) the project to the Nexus repository using standard maven toolchain if the build was successful

Refer to the script source code for configuring the script in regards to the git credentials, maven repository url and maven build options.

Example invocation:

```
/simple_build_script.sh http://192.168.0.100:3000/erouvas/simplerules_dm771.git

Cloning into 'simplerules_dm771'...
remote: Enumerating objects: 98, done.
remote: Counting objects: 100% (98/98), done.
remote: Compressing objects: 100% (44/44), done.
remote: Total 98 (delta 23), reused 94 (delta 21)
Receiving objects: 100% (98/98), 14.81 KiB | 2.47 MiB/s, done.
Resolving deltas: 100% (23/23), done.
[INFO] Scanning for projects...
[INFO] 
[INFO] -------------< com.example.simple_rule:SimpleRules_dm771 >--------------
[INFO] Building SimpleRules_dm771 2.2-SNAPSHOT
[INFO] --------------------------------[ kjar ]--------------------------------
[INFO] 
...
...
Uploading to nexus_local_release: http://192.168.0.100:8081/repository/maven-releases/com/example/simple_rule/SimpleRules_dm771/maven-metadata.xml
Uploaded to nexus_local_release: http://192.168.0.100:8081/repository/maven-releases/com/example/simple_rule/SimpleRules_dm771/maven-metadata.xml (900 B at 2.8 kB/s)
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  8.752 s
[INFO] Finished at: 2021-02-12T04:18:51Z
[INFO] ------------------------------------------------------------------------
::
:: NEW ARTEFACT MAVEN VERSION IS 20210212.007
::
:: COMPLETE ARTEFACT MAVEN VECTOR IS com.example.simple_rule:SimpleRules_dm771:20210212.007
::
:: WARNING: NOT PUSHING TO ORIGINAL GIT REPO
::          GIT REPO VERSION WILL REMAIN UNCHANGED
::
```

The output of the script `com.example.simple_rule:SimpleRules_dm771:20210212.007` can be used as input to the `update-rules.js` script in order to deploy the KJAR to a KIE Server


## Overview of the Update rules script

The `update-rules.js` script will deploy a KJAR to a KIE Container through Decision Central (or headless controller) to all of the managed KIE Servers that belong to a specific KIE Server Group.

This `update-rules.js` script provides a simplistic implementation of a deployment procedure and can only handle one KIE Server Group and one Decision Central.

> **WARNING** The script is based on the Nashorn Javascript engine that is available in Java.8 and Java.11. In Java.11 a depreciation notice is displayed, but can be safely ignored given the scope of the script.

The script is invoked with the following command line arguments:

* the KIE Server ID that the KJAR will be deployed to
* the name of the KIE Container that will be created
* the maven coordinates of the KJAR to be deployed following the `group:artefact:version` format

Further configuration, such as the URL of the Decision Central to be used, required credentials, etc, by supplying the proper values to the script variables.

Example invocation (in the following the `com.example.simple_rule:SimpleRules_dm771:20210212.007` part is provided by the simple_build_script)

```
./update-rules.js remote-kieserver:geo_location:com.example.simple_rule:SimpleRules_dm771:20210212.007

--- BEGIN
Test PASSED : EAP is available at http://192.168.0.100:8080
Test PASSED : BPMS / Business-Central is reachable

Testing available KIE Execution Servers for this controller...
Test PASSED
Server Details:
	         ID:remote-kieserver
	       Name:remote-kieserver
	 TemplateID:remote-kieserver

Deleting container geo_location on the Controller
ResponseCode: [204]

Trying to creating container geo_location at Business Central ...
Test PASSED

Associating with remote server ... 
geo_location updated with com.example.simple_rule:SimpleRules_dm771:20210212.007
--- END-RUN
```



> Written with [StackEdit](https://stackedit.io/).
