#!/bin/bash

# -----------------------------------------------
# -- start of configuration
#
# WARNING: spaces or special chars in directory, spaces and project names NOT supported
#
#
# NOTE: LDAP config: https://access.redhat.com/solutions/3817851
#

#
# - location of configuration file
#
CONFIG_FILE=project_template.config

#
# -- end of configuration
# -----------------------------------------------

if [[ ! -r "$CONFIG_FILE" ]]; then
  echo 'ERROR'
  echo "ERROR: Configuration file $CONFIG_FILE cannot be found - ABORTING"
  echo 'ERROR'
  exit 1
else
  source "$CONFIG_FILE"
fi

source $GIT_REPO_CREDENTIALS

# declare -p GIT_PRJ
# for key in "${!GIT_PRJ[@]}"; do
#     echo "$key = ${GIT_PRJ[$key]}"
# done
# 
# exit

# precompute authorization string
user_auth="$(echo -n "$pamAdmin:$pamPass" | base64)"
default_user_auth="$user_auth"

#
# urlencode/urldecode scripts 
#
urlencode() {
    old_lc_collate=$LC_COLLATE
    LC_COLLATE=C
    
    local length="${#1}"
    for (( i = 0; i < length; i++ )); do
        local c="${1:i:1}"
        case $c in
            [a-zA-Z0-9.~_-]) printf "$c" ;;
            *) printf '%%%02X' "'$c" ;;
        esac
    done
    
    LC_COLLATE=$old_lc_collate
}

urldecode() {
    local url_encoded="${1//+/ }"
    printf '%b' "${url_encoded//%/\\x}"
}


check_limit() {
  local c=${1:-0}
  [[ $c -gt 100 ]] && echo "ERROR: TOO MANY REPETITIONS - ABORTING" && exit 2
}

project_exists() {
  local result="no"
  local local_space="$1"
  local local_prj="$2"
  local local_jobid="$3"
  local job_status=""
  if [[ -n "$local_space" ]] && [[ -n "$local_prj" ]]; then
    job_status=$(curl -k -s --header "authorization: Basic $user_auth" \
                            --header 'Accept: application/json' --header 'content-type: application/json' \
                            -X GET $BC_URL/jobs/$local_jobid | jq -r '.status')
    [[ "$job_status" == "SERVER_ERROR" ]] && result="error"
    if [[ "$result" != "error" ]]; then
      local response=$(curl -k -s --header "authorization: Basic $user_auth" \
                        -H 'Accept: application/json' -H 'content-type: application/json' \
                        -X GET $BC_URL/spaces/$local_space/projects)
      local uris=$(echo -n $response | jq -r '.[].name')
      for u in $uris; do
        [[ "$u" == "$local_prj" ]] && result="yes" && break
      done
    fi
  fi
  echo $result
}

space_exists() {
  local result="no"
  local local_space="$1"
  if [[ -n "$local_space" ]]; then
    local uris=$(curl -k -s -u "$pamAdmin:$pamPass" \
                      -H 'Accept: application/json' -H 'content-type: application/json' \
                      -X GET $BC_URL/spaces | jq -r '.[].name')
    for u in $uris; do
      [[ "$u" == "$local_space" ]] && result="yes" && break
    done
  fi
  echo $result
}

#
# check environment
#
command -v curl &> /dev/null || { echo >&2 'ERROR: CURL not installed. Please install CURL to continue - Aborting'; exit 1; }
command -v jq &> /dev/null || { echo >&2 'ERROR: JQ not installed. Please install JQ ( https://stedolan.github.io/jq/ ) to continue - Aborting'; exit 1; }

response=$(curl -k -s --request GET \
                --url $BC_URL/controller/management/servers \
                --header 'accept: application/json' \
                --header "authorization: Basic $user_auth" \
                --header 'content-type: application/json')
[[ -z "$response" ]] && echo "ERROR: Business Central cannot be found for $BC_URL" && exit 2
[[ "$response" == "${response/server-template/}" ]] && echo "ERROR: Business Central cannot be found for $BC_URL" && exit 1

counter=0

prj_counter=0
has_more=yes
while [[ "$has_more" == "yes" ]]; do
  prj_counter=$((prj_counter+1))
  p="GIT_PRJ_URL_"$prj_counter             && GIT_PRJ_URL="${!p}"
  p="PRJ_NAME_"$prj_counter                && prj_name="${!p}"
  s="PRJ_SPACE_"$prj_counter               && space_name="${!s}"
  g="PRJ_SPACE_DEFAULT_GROUP_"$prj_counter && default_group="${!g}"
  v="PRJ_USER_NAME_"$prj_counter           && prj_user_name="${!v}"
  v="PRJ_USER_PASSWD_"$prj_counter         && prj_user_passwd="${!v}"
  if [[ ! -z $GIT_PRJ_URL ]]; then
    echo
    echo "Working for $prj_name with $GIT_PRJ_URL"
    echo
	  #
	  # delete the project first
	  #
    echo "Deleting project $prj_name"; echo
	  response=$(curl -k -s -u "$pamAdmin:$pamPass" \
                    -H 'Accept: application/json' -H 'content-type: application/json' \
                    -X DELETE $BC_URL/spaces/$space_name/projects/$prj_name)
    if [[ -n "$response" ]]; then
      jobid=$(echo -n $response | jq -r '.jobId')
      job_status=$(echo -n $response | jq -r '.status')
	    sleep 2s
      goon=$(project_exists $space_name $prj_name $jobid) && counter=0
      while [[ "$goon" != "yes" ]]; do
        counter=$((counter+1))
        [[ $(project_exists $space_name $prj_name $jobid) == "no" ]] && echo "[ $counter ] Waiting for [ ${space_name}/${prj_name} ] to be deleted..." && goon="yes" && break
        sleep 2s
      done
    fi
    echo "[ $counter ] Project [ ${space_name}/${prj_name} ] deleted"
    #
    # create the space
    #
    cat << __PAYLOAD > payload
    {
                "name" : "$space_name",
         "description" : "Space for $space_name",
               "owner" : "$pamAdmin",
      "defaultGroupId" : "$default_group"
    }
__PAYLOAD
    echo; echo "Creating space $space_name"; echo
    response=$(curl -k -s -u "$pamAdmin:$pamPass" \
                    -H 'Accept: application/json' -H 'content-type: application/json' \
                    -X POST ${BC_URL}/spaces -d @payload)
    rm -f payload
    sleep 2s
    goon=`space_exists $space_name` && counter=0
    while [[ "$goon" != "yes" ]]; do
      counter=$((counter+1))
      [[ `space_exists $space_name` == "no" ]] && echo "[ $counter ] Waiting for [ ${space_name} ] to be created..." && goon="yes" && break
      sleep 2s
    done
    echo "[ $counter ] Space [ ${space_name} ] created"
	  #
    # go on creating the project
	  #
    clone_user="$GIT_USER_NAME" && [[ -n "$prj_user_name" ]]   && clone_user="$prj_user_name"
    clone_pass="$GIT_PASSWD"    && [[ -n "$prj_user_passwd" ]] && clone_pass="$prj_user_passwd"
    cat << __MYPROJECT > create.project
    {
      "name": "$prj_name",
      "description": "$prj_name",
      "userName": "$clone_user",
      "password": "$clone_pass",
      "gitURL": "$GIT_PRJ_URL"
    }
__MYPROJECT
    echo; echo "Creating project $prj_name"; echo 
    response=$(curl -k -s -u "$pamAdmin:$pamPass" \
                    -H 'Accept: application/json' -H 'content-type: application/json' \
                    -X POST $BC_URL/spaces/$space_name/git/clone -d @create.project)
    jobid=$(echo -n $response | jq -r '.jobId')
    job_status=$(echo -n $response | jq -r '.status')
    rm -f create.project
    # give it a chance to be cloned...
    sleep 3s
    goon=no && counter=0
    while [[ "$goon" != "yes" ]]; do
      counter=$((counter+1))
      response=`project_exists $space_name $prj_name $jobid`
      [[ "$response" == "yes" ]] && echo "[ $counter ] Project [ ${space_name}/${prj_name} ] cloning completed... continuing" && goon="yes" && break
      [[ "$response" == "error" ]] && echo "[ $counter ] ERROR: Project [ ${space_name}/${prj_name} ] cannot be cloned - ABORTING" && break
      [[ "$goon" == "no" ]] && echo "[ $counter ] Waiting for project [ ${space_name}/${prj_name} ] to be cloned... " && sleep 2s
    done
    echo
  else
    has_more=no
  fi
done
